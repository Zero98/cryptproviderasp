using Microsoft.AspNetCore.Http;
using СryptProviderAsp.Services;

namespace СryptProviderAsp.ViewModels
{
    public class EncodeViewModel : AbstracteEcryptionViewModel
    {   
        public void SetTextFromTextArea(string token, string text)
        {
            if(!FileWorker.TrySaveFileTxt(token, text))
            {
                Status = "Не удалось записать информацию в файл!";
            }
            Text = text;
        }
    }
}