﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using СryptProviderAsp.Services;

namespace СryptProviderAsp.ViewModels
{
    public abstract class AbstracteEcryptionViewModel
    {
        public string Text { get; protected set; }
        public string Status { get; protected set; }

        public int Step { get; set; }
        public bool IsRotationLeft {get;set;}


        public virtual void ReadFromFile(IFormFile uploadedFile, string token)
        {
            Text = FileWorker.TryReadFromFile(uploadedFile);
            if (!FileWorker.TrySaveFileTxt(token, Text))
                Status = "Не удалось загрузить файл!";
        }

        public virtual void MakeEncryption(bool isRotationLeft, int step, string token)
        {
            IsRotationLeft = isRotationLeft;
            Step = step;

            GetTextFromServer(token);
            try
            {
                int lastStep = FileWorker.GetLastStep(token);

                FileWorker.SaveStep(token, step);

                if (Math.Abs(lastStep - step) != 0)
                {
                    Text = EncryptionService.Encrypt(isRotationLeft,
                        step - lastStep, Text);
                }
                else if ((lastStep - step) == 0 && lastStep == 32)
                {
                    GetTextFromServer(token);
                }
                else
                {
                    Text = EncryptionService.Encrypt(isRotationLeft, step, Text);
                    Text = EncryptionService.Encrypt(isRotationLeft, step, Text);
                }

                FileWorker.TrySaveFileTxt(token, Text);
            }
            catch(Exception e)
            {
                Status = e.Message;
            }
        }

        public virtual void SaveStep(string token, int step) => 
            FileWorker.SaveStep(token, step);

        public virtual void GetTextFromServer(string token)
        {
            Text = FileWorker.GetTextByToken(token);
            if (string.IsNullOrWhiteSpace(Text))
                Status = "Вы не загрузили файл на сервер";
        }

        public virtual Stream DownloadFile(string token, string docType)
        {
            var FileStream = FileWorker.DownloadFileByToken(token, docType);
            if (FileStream == null)
                Status = "Во время загрузки произошла ошибка";
            return FileStream;
        }

        public bool IsDocxFile(string filename) 
            => filename.Split('.').Last().ToLower() == "docx" ? true : false;

        public bool IsTxtFile(string filename)
            => filename.Split('.').Last().ToLower() == "txt" ? true : false;

    }
}
    