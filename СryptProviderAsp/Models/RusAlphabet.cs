﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace СryptProviderAsp.Models
{
    public static class RusAlphabet
    {
        private static char[] completeAlphabet = new[]
        {
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        };

        private static Dictionary<char, int> lowerAlphabetTable;
        private static Dictionary<char, int> upperAlphabetTable;

        public static void CreateAlphabet()
        {
            if (lowerAlphabetTable == null && upperAlphabetTable == null)
            {
                lowerAlphabetTable = new Dictionary<char, int>();
                upperAlphabetTable = new Dictionary<char, int>();

                for (int i = 0; i < completeAlphabet.Length; i++)
                {
                    lowerAlphabetTable.Add(char.ToLower(completeAlphabet[i]), i);
                    upperAlphabetTable.Add(completeAlphabet[i], i);
                }
            }
        }

        public static bool ContainsSymbol(char symbol)
        {
            CreateAlphabet();
            return lowerAlphabetTable.ContainsKey(symbol) ||
            upperAlphabetTable.ContainsKey(symbol);
        }

        public static char GetSymbolWithStep(int step, char currentSymbol)
        {
            CreateAlphabet();
            var newChar = '\0';
            if (lowerAlphabetTable.ContainsKey(currentSymbol))
            {
                var index = lowerAlphabetTable[currentSymbol];
                int newIndex = (index + step) % 33;
                if (newIndex < 0)
                    newIndex = 33 + newIndex;
                newChar = lowerAlphabetTable.First(n => n.Value == newIndex).Key;
            }
            else
            {
                var index = upperAlphabetTable[currentSymbol];
                int newIndex = (index + step) % 33;
                if (newIndex < 0)
                    newIndex = 33 + newIndex;
                newChar = upperAlphabetTable.First(n => n.Value == newIndex).Key;
            }

            return newChar;
        }
    }
}
