using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using SautinSoft.Document;

namespace СryptProviderAsp.Services
{
    public static class FileWorker
    {
        public static string TryReadFromFile(IFormFile uploadedFile)
        {
            try
            {
                string textFromFile = "";
                switch (uploadedFile.FileName.Split('.').Last().ToLower())
                {
                    case "docx":
                        {
                            textFromFile = ReadFromDocxFile(uploadedFile);
                            break;
                        }
                    case "txt":
                        {
                            textFromFile = ReadFromTxtFile(uploadedFile);
                            break;
                        }
                    default:
                        {
                            throw new Exception("Некорректный формат файла!");
                        }
                }

                return textFromFile;
            }
            catch (Exception e) { }

            return null;
        }

        public static void SaveStep(string token, int newStep)
        {
            var filePath = Directory.GetCurrentDirectory() + "/Files/step" + token + ".txt";
            if (!File.Exists(filePath))
            {
                using (var sw = new StreamWriter(File.Create(filePath)))
                {
                    sw.WriteLine(newStep);
                }
            }
            else
            {
                using (var sw = new StreamWriter(File.Create(filePath)))
                {
                    sw.WriteLine(newStep);
                }
            }
        }

        public static int GetLastStep(string token)
        {
            var filePath = Directory.GetCurrentDirectory() + "/Files/step" + token + ".txt";
            var step = 0;
            if (File.Exists(filePath))
            {
                using (var sr = new StreamReader(File.Open(filePath, FileMode.OpenOrCreate)))
                {
                    if (!int.TryParse(sr.ReadLine(), out step))
                    {
                        throw new Exception("Некоррекнтый файл");
                    }
                }
            }
            return step;
        }

        public static Stream DownloadFileByToken(string token, string docType)
        {
            var filePath = Directory.GetCurrentDirectory() + "/Files/" + token;

            if (!File.Exists(filePath + ".txt"))
                return null;
            try
            {
                switch (docType)
                {
                    case "docx":
                        {
                            if(!File.Exists(filePath + ".docx"))
                            {
                                DocumentCore dc = DocumentCore.Load(filePath + ".txt");
                                dc.Save(filePath + ".docx");
                            }

                            return File.Open(filePath + ".docx", FileMode.Open); 
                        }
                    case "txt":
                        {
                            return File.Open(filePath + ".txt", FileMode.Open);
                        }
                    default:
                        {
                            return null;
                        }
                }   
            }
            catch(Exception e) { }

            return null;
        }

        public static string GetTextByToken(string token)
        {
            var filePath = Directory.GetCurrentDirectory() + "/Files/" + token + ".txt";

            if (!File.Exists(filePath))
                return null;

            try
            {
                var text = "";
                using (var streamReader
                    = new StreamReader(Directory.GetCurrentDirectory() + "/Files/" + token+".txt"))
                {
                    text = streamReader.ReadToEnd();
                }

                return text;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static bool TrySaveFileTxt(string token, string text)
        {
            try
            {
                if (!Directory.Exists(Directory.GetCurrentDirectory() + "/Files"))
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/Files");
                using (var streamWriter
                    = new StreamWriter(Directory.GetCurrentDirectory() + "/Files/" + token + ".txt"))
                {
                    streamWriter.Write(text);
                }
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        private static string ReadFromTxtFile(IFormFile uploadedFile)
        {
            var textFromFile = "";
            using (var streamReader = new StreamReader(uploadedFile.OpenReadStream()))
            {
                textFromFile = streamReader.ReadToEnd();
            }

            return textFromFile;
        }

        private static string ReadFromDocxFile(IFormFile uploadedFile)
        {
            var dc = DocumentCore.Load(uploadedFile.OpenReadStream(), new DocxLoadOptions());

            return dc.Content.ToString();
        }
    }
}