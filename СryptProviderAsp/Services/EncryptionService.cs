﻿using System.Text;
using System.Linq;
using System.Collections;
using СryptProviderAsp.Models;
using System;

namespace СryptProviderAsp.Services
{
    public static class EncryptionService
    {
        public static string Encrypt(bool isRotationLeft, int step, string text)
        {
            StringBuilder newText = new StringBuilder();

            foreach(char symbol in text)
            {
                if (RusAlphabet.ContainsSymbol(symbol))
                {
                    newText.Append(ShiftByStep(symbol, step, isRotationLeft));
                }
                else
                {
                    newText.Append(symbol);
                }
            }

            return newText.ToString();
        }

        private static char ShiftByStep(char changeableSymbol, int step, bool isRotationLeft)
        {
            if (!isRotationLeft)
            {
                return RusAlphabet.GetSymbolWithStep(step, changeableSymbol);
            }

            return RusAlphabet.GetSymbolWithStep(-step, changeableSymbol);
        }
    }
}
