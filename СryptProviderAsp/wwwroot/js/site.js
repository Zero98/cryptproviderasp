﻿function uploadValidat() {
    var file = $("#uploadedFile").val();
    $("#error").empty()
    if (file == "") {
        $('<p style="color:red">Укажите файл!</p>').appendTo("#error")
        return false
    }
    else {
        return true;
    }
}    

function downloadValidat() {
    var filename = $("#filename-field").val();
    $("#error").empty()
    if (filename == "") {
        $('<p style="color:red">Укажите имя файла с одним из доступных рассширений!</p>').appendTo("#error")
        return false
    }
    else if (!(/\S+\.((docx)|(txt))(?![\S])/g.test(filename))){
        $('<p style="color:red">Введите имя файл в требуемом формате!</p>').appendTo("#error")
        return false
    }
    else {
        return true;
    }
}

function leftStep(action) {
    $("#error").empty()
    var text = $("#text-form").val();

    if (text == "") {
        $('<p style="color:red">Пожалуйста, выберете файл!</p>').appendTo("#error")
        $("#shifter").val(0)
    }
    else {
        var step = $("#shifter").val();
        if (step > 0) {
            step--;

            $("#shifter").val(step);
            var isRotationLeft = $("#cbx").val() == 'on';

            $.ajax({
                type: "POST",
                url: "/Home/" + action + "/",
                data: { isRotationLeft: isRotationLeft, step: step, text: text },
                dataType: "json",
                success: function (msg) {
                    $("#text-form").val(msg)
                }
            });
        }
    }
}

function rightStep(action) {
    $("#error").empty()
    var text = $("#text-form").val();

    if (text == "") {
        $('<p style="color:red">Пожалуйста, выберете файл!</p>').appendTo("#error")
        $("#shifter").val(0)
    }
    else {
        var step = $("#shifter").val()
        if (step < 33) {
            step++;
            $("#shifter").val(step);
            var isRotationLeft = $("#cbx").val() == 'on';

            $.ajax({
                type: "POST",
                url: "/Home/" + action + "/",
                data: { isRotationLeft: isRotationLeft, step: step, text: text },
                dataType: "json",
                success: function (msg) {
                    $("#text-form").val(msg)
                }
            });
        }
    }
}

function updateText(action) {
    $("#error").empty()
    var isRotationLeft = $("#cbx").val() == 'on';
    var step = $("#shifter").val();
    var text = $("#text-form").val();
    
    if (text == "") {
        $('<p style="color:red">Пожалуйста, выберете файл!</p>').appendTo("#error")
        $("#shifter").val(0)
    }
    else {
        $.ajax({
            type: "POST",
            url: "/Home/" + action + "/",
            data: { isRotationLeft: isRotationLeft, step: step, text: text },
            dataType: "json",
            success: function (msg) {
                $("#text-form").val(msg)
            }
        });
    }
}

function changeRotation(action) {
    if ($("#cbx").val() == "off") {
        $("#cbx").add("checked")
        $("#cbx").val("on")
    }
    else {
        $("#cbx").removeAttr("checked")
        $("#cbx").val("off")
    }
    updateText(action)
}
