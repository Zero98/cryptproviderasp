﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using СryptProviderAsp.Models;
using СryptProviderAsp.ViewModels;

namespace СryptProviderAsp.Controllers
{
    public class HomeController : Controller
    {
        private IHostingEnvironment appEnvironment;

        private EncodeViewModel encodeViewModel = new EncodeViewModel();
        private DecodeViewModel decodeViewModel;

        public HomeController(IHostingEnvironment appEnvironment)
        {
            this.appEnvironment = appEnvironment;
            decodeViewModel = new DecodeViewModel();
        }
        
        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public IActionResult DecodePage()
        {      
            return View(decodeViewModel);
        }
        
        [HttpGet]
        public IActionResult EncodePage()
        {   
            return View(encodeViewModel);
        }

        [HttpPost]
        public IActionResult UploadDecode(IFormFile uploadedFile)
        {
            return Upload(uploadedFile, decodeViewModel, "DecodePage");
        }

        [HttpPost]
        public IActionResult UploadEncode(IFormFile uploadedFile)
        {
            return  Upload(uploadedFile, encodeViewModel, "EncodePage");
        }

        private IActionResult Upload(IFormFile uploadedFile, AbstracteEcryptionViewModel viewModel, string viewName)
        {
            var token = "";
            if (uploadedFile != null
                && Request.Cookies.TryGetValue(".AspNetCore.Antiforgery.ofwCqZFQY_4", out token))
            {
                viewModel.ReadFromFile(uploadedFile, token);
                viewModel.SaveStep(token, 0);
                viewModel.Step = 0;
                viewModel.IsRotationLeft = false;
            }
            
            return View(viewName, viewModel);
        }

        [HttpPost]
        public IActionResult DownloadDecode(string filename)
        {
            return DownLoad(filename, decodeViewModel, "DecodePage");
        }

        [HttpPost]
        public IActionResult DownloadEncode(string filename)
        {
            return DownLoad(filename, encodeViewModel ,"EncodePage");
        }

        private IActionResult DownLoad(string filename, AbstracteEcryptionViewModel viewModel, string viewName)
        {
            var token = "";
            Request.Cookies.TryGetValue(".AspNetCore.Antiforgery.ofwCqZFQY_4", out token);
            if (viewModel.IsDocxFile(filename))
            {
                var fileStream = viewModel.DownloadFile(token, "docx");
                if (fileStream == null)
                {
                    return View(viewName, viewModel);
                }
                return File(fileStream,
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
            }
            else if (viewModel.IsTxtFile(filename)) 
            {
                var fileStream = viewModel.DownloadFile(token, "txt");
                if (fileStream == null)
                {
                    return View(viewName, viewModel);
                }

                return File(fileStream, "application/txt", filename);
            }
            else
            {
                return View(viewName, viewModel);
            }
        }

        [HttpPost]
        public JsonResult Decode(bool isRotationLeft, int step)
        {
             var token = "";
            Request.Cookies.TryGetValue(".AspNetCore.Antiforgery.ofwCqZFQY_4", out token);

            decodeViewModel.MakeEncryption(isRotationLeft, step, token);

            return Json(decodeViewModel.Text);
        }

        [HttpPost]
        public JsonResult Encode(bool isRotationLeft, int step, string text)
        {
            var token = "";
            Request.Cookies.TryGetValue(".AspNetCore.Antiforgery.ofwCqZFQY_4", out token);

            encodeViewModel.SetTextFromTextArea(token, text);

            encodeViewModel.MakeEncryption(isRotationLeft, step, token);

            return Json(encodeViewModel.Text);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}