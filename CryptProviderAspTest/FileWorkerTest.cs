﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using SautinSoft.Document;
using СryptProviderAsp.Services;
using System.Linq;
using СryptProviderAsp.Models;

namespace CryptProviderAspTest
{
    [TestClass]
    public class FileWorkerTest
    {
        private const string ACTUALTESTTEXT = "Я мою Сашу";
        private const string TESTTOKEN = "testtoken";
        private const string DOCXTYPE = "docx";
        private const string TXTTYPE = "txt";
        private const string ACTUALDOCXTEXT = "Я мою Сашу\r\nCreated by the trial version of Document .Net 3.6.11.20!\r\nThe trial version sometimes inserts \"trial\"" +
            " into random places.\r\nGet the full version of Document .Net.\r\n";
        private const int ACTUALSTEP = 1;
        [TestMethod]
        public void SaveTextToTXT()
        {
            if (FileWorker.TrySaveFileTxt(TESTTOKEN, ACTUALTESTTEXT))
            {
                var expected = FileWorker.GetTextByToken(TESTTOKEN);

                Assert.AreEqual(expected, ACTUALTESTTEXT);
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CheckGettingDocxFile()
        {
            using (var stream = FileWorker.DownloadFileByToken(TESTTOKEN, DOCXTYPE))
            {
                DocumentCore dc = DocumentCore.Load(stream, LoadOptions.DocxDefault);

                var expected = dc.Content.ToString();
                Assert.AreEqual(expected, ACTUALDOCXTEXT);
            }
        }

        [TestMethod]
        public void CheckGettingTxtFile()
        {
            string expected = "";
            using (var stream = new StreamReader(FileWorker.DownloadFileByToken(TESTTOKEN, TXTTYPE)))
            {
                expected = stream.ReadLine();
                
            }
            Assert.AreEqual(expected, ACTUALTESTTEXT);
        }

        [TestMethod]
        public void CheckLastStep()
        {
            FileWorker.SaveStep(TESTTOKEN, ACTUALSTEP);
            var expected = FileWorker.GetLastStep(TESTTOKEN);
            Assert.AreEqual(expected, ACTUALSTEP);
        }
    }
}
