﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using СryptProviderAsp.Services;

namespace CryptProviderAspTest
{
    [TestClass]
    public class EncryptTests
    {
        private const bool ISLEFTROTATION = true;
        private const bool ISRIGHTROTATION = false;

        private const string ACTUALENDTESTTEXT = "Я мою Сашу";

        [TestMethod]
        public void TestEncryptRightRotation1()
        {
            var actualBeginTestText = "Ю лнэ Рячт";
            var expected = EncryptionService.Encrypt(ISRIGHTROTATION, 1, actualBeginTestText);
            Assert.AreEqual(expected, ACTUALENDTESTTEXT);
        }

        [TestMethod]
        public void TestEncrypLeftRotation1()
        {
            var actualBeginTestText = "А нпя Тбщф";
            var expected = EncryptionService.Encrypt(ISLEFTROTATION, 1, actualBeginTestText);
            Assert.AreEqual(expected, ACTUALENDTESTTEXT);
        }

        [TestMethod]
        public void TestEncrypRightRotationAndThenLeftRotationChange()
        {
            var actualBeginTestText = "Ю лнэ Рячт";
            var expected = EncryptionService.Encrypt(ISRIGHTROTATION, 1, actualBeginTestText);
            expected = EncryptionService.Encrypt(ISLEFTROTATION, 1, expected);

            Assert.AreEqual(expected, actualBeginTestText);
        }
    }
}
